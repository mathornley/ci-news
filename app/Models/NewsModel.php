<?php namespace App\Models;

use CodeIgniter\Model;

class NewsModel extends Model
{
	// The table we're using in this model.
    protected $table = 'news';

    // This for mass assignment whilst saving data to the table.
    protected $allowedFields = ['title', 'slug', 'body'];

    public function getNews($slug = false)
	{
	    if ($slug === false)
	    {
	        return $this->findAll();
	    }

	    return $this->asArray()
	                ->where(['slug' => $slug])
	                ->first();

	    // The two methods used here, findAll() and first(), are provided by the Model class
	}
}