<?php namespace App\Controllers;

use CodeIgniter\Controller;

class Pages extends Controller // Extends (system/Controller.php
{
    public function index()
    {
        return view('welcome_message');
    }

    public function view($page = 'home')
	{
	    if (!is_file(APPPATH.'/Views/pages/'.$page.'.php'))
	    {
	        // Whoops, we don't have a page for that!
	        throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
	    }

	    $data['title'] = 'The '.ucfirst($page). ' page'; // Capitalize the first letter

	    echo view('templates/header', $data);
	    echo view('pages/'.$page, $data);
	    echo view('templates/footer', $data);
	}

	 public function shop()
	 {
	    echo 'This is where shop will go once it\'s built';
	 }
}