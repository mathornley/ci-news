<?php namespace App\Controllers;

use App\Models\NewsModel;
use CodeIgniter\Controller;

class News extends Controller
{

	/**
	 * Instantiate the model "news" first.
	 */
	public function __construct()
	{
		// Connect to the NewsModel Model
		$this->model = new NewsModel();	
	}

	/**
	 * Get all the news article
	 * @return [type] [description]
	 */
    public function index()
	{
	    $data = [
	        'news'  => $this->model->getNews(),
	        'title' => 'News archive',
	    ];

	    echo view('templates/header', $data);
	    echo view('news/overview', $data);
	    echo view('templates/footer', $data);
	}

	/**
	 * Get the news article based on the slug passed in the URL.
	 * @param  [type] $slug [description]
	 * @return [type]       [description]
	 */
    public function view($slug = NULL)
	{
	    $data['news'] = $this->model->getNews($slug);

	    if (empty($data['news']))
	    {
	        throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the news item: '. $slug);
	    }

	    $data['title'] = $data['news']['title'];

	    echo view('templates/header', $data);
	    echo view('news/view', $data);
	    echo view('templates/footer', $data);
	}

	/**
	 * Try and edit the article
	 * @param  boolean $slug [description]
	 * @return [type]        [description]
	 */
	public function edit($slug = false)
	{
		// Make sure we've a slug
		if ($slug === false)
	    {
	        throw new \CodeIgniter\Exceptions\PageNotFoundException('No slug and one is needed');
	    }

	   	// Get the article data so we can populate the form
	   	$data = $this->model->getNews($slug);

	    echo view('templates/header', ['title' => 'Edit a news item: '.$data['title']]);
        echo view('news/create', $data);
        echo view('templates/footer');
	}

	/**
	 * Add a news article to the database.
	 * @return [type] [description]
	 */
	public function create()
	{
	    if ($this->request->getMethod() === 'post' && $this->validate([
	            'title' => 'required|min_length[3]|max_length[255]',
	            'body'  => 'required'
	        ]))
	    {
	        $this->model->save([
	            'title' => $this->request->getPost('title'),
	            'slug'  => url_title($this->request->getPost('title'), '-', TRUE),
	            'body'  => $this->request->getPost('body'),
	        ]);

	        echo view('news/success');

	    }
	    else
	    {
	        echo view('templates/header', ['page_title' => 'Create a news item']);
	        echo view('news/create');
	        echo view('templates/footer');
	    }
	}

 }