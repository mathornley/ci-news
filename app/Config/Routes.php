<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false); // If set to false, autorouting is off and routes have to be specified.

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default


// Get the welcome page.
$routes->get('/', 'Home::index');
//$routes->get('/pages/shop', 'Pages::shop'); // A standard route if routes are switched off.
//$routes->get('(:any)', 'Pages::view/$1');

// Get the add a news article create form and post it
$routes->match(['get','post'], 'news/create', 'News::create');

// Edit a route - get the edit form
$routes->get('news/edit/(:segment)', 'News::edit/$1');

// Edit a route - get the edit form
$routes->post('news/update/(:segment)', 'News::update/$1');

// Get all the news articles. 
$routes->get('/news', 'News::index');

// Get a news article by slug. 
$routes->get('news/(:segment)', 'News::view/$1');
//$routes->get('news/create', 'News::view/$1');


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
