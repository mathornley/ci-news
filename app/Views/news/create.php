<?=$id?>

<h2><?= isset($page_title) ?  esc($page_title) : ''; ?></h2>

<!--  function is used to report errors related to form validation -->
<?= \Config\Services::validation()->listErrors(); ?>

<form action="/news/create" method="post">

	<!-- . The csrf_field() function creates a hidden input with a CSRF token that helps protect against some common attacks. -->
    <?= csrf_field() ?>

    <p><label for="title">Title</label>
    <input type="input" name="title" value="<?=isset($title) ? $title : '';?>" /><br /></p>

    <p><label for="body">Text</label>
    <textarea name="body"><?=isset($body) ? $body : '';?></textarea><br /></p>

	<p><input type="submit" name="submit" value="Create news item" /></p>

</form>