<!doctype html>
<html>
<head>
    <title>CodeIgniter Tutorial</title>
</head>
<body>

    <h1><?= isset($title) ? esc($title) : ''; ?></h1>

<!-- 
	If you look closely in header.php template we are using an esc() function. It’s a global function provided by CodeIgniter to help prevent XSS attacks. You can read more about it here.
-->